////////// Bootstrap Carousel - START ///////////////////////////////

$('.carousel').carousel({
  interval: 2000
});
////////// Bootstrap Carousel - END ///////////////////////////////


////////// Magnific Popup - START /////////////////////////////////
// Example with multiple objects
$('#btnGallery').magnificPopup({
  items: [
    {
      src: 'images/again.jpg'
    } ,
    {
      src: 'images/air.jpg'
    } ,
    {
      src: 'images/ball.jpg'
    } ,
    {
      src: 'images/blue.jpg'
    } 
  ],
  gallery: {
    enabled: true,
    removalDelay: 300,       // Delay in milliseconds before popup is removed
    mainClass: 'mfp-fade' 
  },
  type: 'image' // this is default type
});
////////// Magnific Popup - END /////////////////////////////////


/////////  Galleria - START  //////////////////////////////////////
(function() { 
  Galleria.loadTheme('galleria/themes/classic/galleria.classic.min.js');
  Galleria.run('#gallery', {
    height: parseInt($('#gallery').css('height')),
    wait: true
   });
}());
/////////  Galleria - END    //////////////////////////////////////











