// LEAFLET MAP -----------------  START --------------------------------------
var map = L.map('map', {
  center: [49.71008, -124.97138],
  //minZoom: 12,
  zoom: 12
});

L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
  attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a>',
  subdomains: ['a', 'b', 'c']
}).addTo(map);

markers = [{
    "name": "MARKER ONE",
    "lat": 49.71,
    "lng": -124.90
  },
  {
    "name": "MARKER TWO",
    "lat": 49.72,
    "lng": -124.99
  },
  {
    "name": "MARKER THREE",
    "lat": 49.73,
    "lng": -124.95
  }
];

for (var i = 0; i < markers.length; ++i) {
  L.marker([markers[i].lat, markers[i].lng])
    .bindPopup('<a href="' + markers[i].url + '" target="_blank">' + markers[i].name + '</a>')
    .addTo(map);
}
// LEAFLET MAP -----------------  END --------------------------------------


// CHARTS -  GRAPH -------------  START --------------------------------------
var ctx = document.getElementById("myChart");
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ["Red", "Blue", "Yellow", "Green", "Purple", "Orange"],
        datasets: [{
            label: '# of Votes',
            data: [12, 19, 3, 5, 2, 3],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});
// CHARTS -  GRAPH -------------  END  --------------------------------------


// CHARTS -  RADAR CHART  -------------  START  -----------------------------
new Chart(document.getElementById("radar-chart"), {
  type: 'radar',
  data: {
    labels: ["Africa", "Asia", "Europe", "Latin America", "North America"],
    datasets: [
      {
        label: "1950",
        fill: true,
        backgroundColor: "rgba(179,181,198,0.2)",
        borderColor: "rgba(179,181,198,1)",
        pointBorderColor: "#fff",
        pointBackgroundColor: "rgba(179,181,198,1)",
        data: [8.77,55.61,21.69,6.62,6.82]
      }, {
        label: "2050",
        fill: true,
        backgroundColor: "rgba(255,99,132,0.2)",
        borderColor: "rgba(255,99,132,1)",
        pointBorderColor: "#fff",
        pointBackgroundColor: "rgba(255,99,132,1)",
        pointBorderColor: "#fff",
        data: [25.48,54.16,7.61,8.06,4.45]
      }
    ]
  },
  options: {
    title: {
      display: true,
      text: 'Distribution in % of world population'
    }
  }
});




// CHARTS -  RADAR CHART  -------------  END  -------------------------------


// D3.js  -  3D Donut -------------  START  -------------------------------
var salesData=[
	{label:"Basic", color:"#3366CC"},
	{label:"Plus", color:"#DC3912"},
	{label:"Lite", color:"#FF9900"},
	{label:"Elite", color:"#109618"},
	{label:"Delux", color:"#990099"}
];

var svg = d3.select("#donut3d").append("svg").attr("width",700).attr("height",300);

svg.append("g").attr("id","salesDonut");
svg.append("g").attr("id","quotesDonut");

Donut3D.draw("salesDonut", randomData(), 150, 150, 130, 100, 30, 0.4);
Donut3D.draw("quotesDonut", randomData(), 450, 150, 130, 100, 30, 0);
	
function changeData(){
	Donut3D.transition("salesDonut", randomData(), 130, 100, 30, 0.4);
	Donut3D.transition("quotesDonut", randomData(), 130, 100, 30, 0);
}

function randomData(){
	return salesData.map(function(d){ 
		return {label:d.label, value:1000*Math.random(), color:d.color};});
}
// D3.js  -  3D Donut -------------  END  -------------------------------


// $("#with-jquery").css("position", "absolute");
// $("#with-jquery").animate({ left: "500px" }, 2000);
var distance = "800px";
var duration = 3000;

function withAnimate() {
  $("#with-jquery").css("position", "absolute");
  $("#with-jquery").animate({ "left": distance}, duration);

  // $("#with-velocity").css("position", "absolute");
  // $("#with-velocity").velocity({ left: distance}, duration);
}

$("#play-jquery").on('click', function(){
  withAnimate();
});

function withVelocity() {
  $("#with-velocity").css("position", "absolute");
  $("#with-velocity").velocity({ left: distance}, duration);
}

$("#play-velocity").on('click', function(){
  withVelocity();
});